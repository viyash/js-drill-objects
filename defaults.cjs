
let iceCream = { flavor: "chocolate", sprinkles: "lots" };

function defaults(obj, defaultProps) {

    for (let key in defaultProps) {
        if (!obj[key]) {
            obj[key] = defaultProps[key]
        }
    }
    return obj
}


let result = defaults({ "flavor": "s" }, iceCream)
console.log(result)
module.exports = defaults