
const data = require('./data.cjs')

function cb(value,key){
    return value+"s"
}

function map(elements, functions) {

    for (let key in elements) {
        elements[key] = functions(elements[key],key)
    }
    return elements
}


let result = map(data,cb)
console.log(result)
module.exports = map
